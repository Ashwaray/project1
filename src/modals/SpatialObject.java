/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modals;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Geometry;

/**
 *
 * @author lennovo
 */
public abstract class SpatialObject {
    protected AssetManager assetManager;
    protected Geometry geom;
    protected RigidBodyControl rbc;
    protected String uri;
    protected ColorRGBA color;
    protected final int mass = 10;

    public SpatialObject(AssetManager assetManager, String uri, ColorRGBA color) {
        this.assetManager = assetManager;
        this.uri = uri;
        this.color = color;
    }
    
    protected abstract void createGeometryObject();
    
    protected abstract void createRBCObject();
}
