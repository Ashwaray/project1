/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modals;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;
import utils.ArenaConstants;

/**
 *
 * @author lennovo
 */
public class Arena {
    public Geometry makeFloor(AssetManager assetManager) {
        Box box = new Box(ArenaConstants.LENGTH, ArenaConstants.HEIGHT, ArenaConstants.BREADTH);
        Geometry floor = new Geometry(ArenaConstants.ARENA, box);
        floor.setLocalTranslation(ArenaConstants.ArenaTranslationX,
                                  ArenaConstants.ArenaTranslationY,
                                  ArenaConstants.ArenaTranslationZ);
        
        Material mat1 = new Material(assetManager, ArenaConstants.MATERIAL_RESOURCE_PATH);
        mat1.setColor(ArenaConstants.MAT_COLOR, ColorRGBA.Gray);
        floor.setMaterial(mat1);
        return floor;
    }
}
