/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modals;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;
import factory.Shape;
import utils.ShapeConstants;


/**
 *
 * @author lennovo
 */
public final class BoxObject extends SpatialObject implements Shape{
    
    private Integer x, y, z;

    public BoxObject(AssetManager _assetManager, Integer x, Integer y, Integer z, 
            String uri, ColorRGBA color) {
        super(_assetManager, uri, color);
        this.x = x;
        this.y = y;
        this.z = z;
        createGeometryObject();
        createRBCObject();
    }
    
    private boolean doesRequirementMeet() {
        if(super.assetManager == null || this.x == null || this.y == null || 
                this.z == null) return false;
        return true;
    }
    
    @Override
    public Geometry getGeometryObject() {
        return super.geom;
    }
    
    @Override
    protected void createGeometryObject() {
        if(!doesRequirementMeet()) throw new RuntimeException("Some null value provided");
        Box b = new Box(this.x, this.y, this.z);
        Geometry geom1 = new Geometry(ShapeConstants.BOX, b);
        Material mat = new Material(super.assetManager, super.uri);
        mat.setColor("Color", super.color);
        geom1.setMaterial(mat);
        super.geom = geom1;
    }
    
    @Override
    protected void createRBCObject() {
        CollisionShape boxcs = CollisionShapeFactory.createBoxShape(this.geom);
        super.rbc = new RigidBodyControl(boxcs, mass);
        super.geom.addControl(this.rbc);
    }

    @Override
    public RigidBodyControl getRBCObject() {
        return super.rbc;
    }

    public Integer getX() {
        return x;
    }

    public Integer getY() {
        return y;
    }

    public Integer getZ() {
        return z;
    }

    public AssetManager getAssetManager() {
        return assetManager;
    }

    public Geometry getGeom() {
        return geom;
    }

    public RigidBodyControl getRbc() {
        return rbc;
    }

    public String getUri() {
        return uri;
    }

    public ColorRGBA getColor() {
        return color;
    }

    public int getMass() {
        return mass;
    } 
}
