/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 *
 * @author lennovo
 */
public class ShapeConstants {
    public static final String BOX = "Box";
    public static final String BOX_MATERIAL_URI = "Common/MatDefs/Misc/Unshaded.j3md";
    public static final String BOX_COLOR = "BoxColor";
    
    public static final String SPHERE = "Sphere";
    public static final String SPHERE_MATERIAL_URI = "Common/MatDefs/Misc/Unshaded.j3md";
    public static final String SPHERE_COLOR = "SphereColor";
}
