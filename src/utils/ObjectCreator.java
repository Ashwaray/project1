/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.collision.shapes.CylinderCollisionShape;
import com.jme3.bullet.collision.shapes.SphereCollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Cylinder;
import com.jme3.scene.shape.Sphere;
import modals.BoxObject;
import modals.SpatialState;

/**
 *
 * @author lennovo
 */
public class ObjectCreator {
    public static SpatialState createSphere(AssetManager assetManager) {
         Sphere s = new Sphere(10, 10, 2);
      Geometry gs = new Geometry("S", s);
      Material ms = new Material(assetManager,
               "Common/MatDefs/Misc/Unshaded.j3md");
      
      ms.setColor("Color", ColorRGBA.Red);
      gs.setMaterial(ms);
      CollisionShape scs = new SphereCollisionShape(10);
      RigidBodyControl srbc = new RigidBodyControl(scs);
      gs.addControl(srbc);
      Vector3f temp = new Vector3f(30, 10, 0);
      srbc.setPhysicsLocation(temp);
      return new SpatialState(1, gs, srbc, temp);
    }
    
    public static SpatialState createCylinder(AssetManager assetManager) {
      Cylinder s = new Cylinder(10, 10, 1, 2);
      Geometry gs = new Geometry("C", s);
      Material ms = new Material(assetManager,
               "Common/MatDefs/Misc/Unshaded.j3md");
      
      ms.setColor("Color", ColorRGBA.Magenta);
      gs.setMaterial(ms);
      CollisionShape scs = new CylinderCollisionShape(new Vector3f(10  , 10, 10));
      RigidBodyControl srbc = new RigidBodyControl(scs);
      gs.addControl(srbc);
      Vector3f temp = new Vector3f(-130, 10, 0);
      srbc.setPhysicsLocation(temp);
      return new SpatialState(2, gs, srbc, temp);
    }
    
    public static SpatialState createBox(AssetManager assetManager) {
        BoxObject boxObj = 
              new BoxObject(assetManager, 1, 1, 1, 
                      "Common/MatDefs/Misc/Unshaded.j3md", ColorRGBA.Blue);
        return new SpatialState(0, boxObj.getGeom(), boxObj.getRbc(), 
                                new Vector3f(0, 0, 0));
    }
}
